Pod::Spec.new do |s|

  s.name         = "OptImagePicker"
  s.version      = "0.3.1"
  s.summary      = "Optimize UIImagePickerController with multiple selection support."
  s.homepage     = "https://bitbucket.org/luuphuoclinh/imagepicker"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.authors      = { "Linh Luu" => "luuphuoclinh@gmail.com" }

  s.platform         = :ios, "8.0"

  s.source       = { :git => "https://bitbucket.org/org:luuphuoclinh/imagepicker.git", :tag => s.version.to_s }  
  s.source_files  = ["OptImagePicker/*.{h,m}"]
  s.exclude_files    = "OptImagePicker/OptImagePicker.h"
  s.resource_bundles = { "OptImagePicker" => "OptImagePicker/*.{lproj,storyboard}" }
  s.requires_arc = true
  s.framework = "Photos"

end
